
# Key bindings

## Equals

1. alt = **modkey**  
2. shift = **shiftmask**  
3. start = **mod4mask**  
4. enter = **return**  
5. ctrl = **controlmask**  

## Moving without mouse  

| Keys                       | Usage
|---                         |---
| **alt + 1,2,3,4...**           |move through the different tags
| **alt + j**                    |move focus to the right
| **alt + k**                    |move focus to the left
| **alt + 0**                    |see all the tags together at the same time
| **alt + ,**                    |focus on the other monitor
| **alt + .**                    |focus on the other monitor
| **alt + shift + ,**            |move the current window to the other monitor
| **alt + shift + .**            |move the current window to the other monitor
| **alt + shift + 0**            |make the window follow you
| **alt + shift + 1**            |stop following and move to 1
| **alt + i**                    |windows splitted horizontally
| **alt + d**                    |windows splitted vertically

## Spawing/Quiting  

| Keys                       | Usage
|---                         |---
| **alt + shift + enter**        |spawn terminal (st)
| **alt + p**                    |spawn dmenu
| **print-screen**               |run my screenshot script from .local/bin screenshot
| **alt + shift + q**            |quit dwm

## Resizing  

| Keys                       | Usage
|---                         |---
| **alt + h**                    |increase the size of the right windows, gaps same
| **alt + l**                    |increase the size of the left windows, gaps same
| **alt + shift + c**            |kill the window
| **alt + tab**                  |change the view between the 2 most res windows
| **alt + start + 0**            |make the gaps 0
| **alt + start + shift + 0**    |make the gaps default again
| **alt + b**                    |hide the dwmstatus bar on top
| **alt + start + l**            |increase all gaps evenly
| **alt + start + h**            |decrease all gaps evenly
| **alt + start + shift + h**    |decrease the size of all windows
| **alt + start + shift + l**    |increase the size of all windows
| **alt + start + ctrl + h**     |increase the gaps without moving the corners
| **alt + start + ctrl + l**     |decrease the gaps without moving the corners
| **alt + y**                    |increase the gaps of the current window
| **alt + o**                    |decrease the gaps of the current window
| **alt + ctrl + y**             |increase the middle gap
| **alt + ctrl + o**             |decrease the middle gap
| **alt + start + y**            |increase the gaps on the bot/top corners only
| **alt + start + o**            |decrease the gaps on the bot/top corners only
| **alt + shift + y**            |increase the gaps on the left/right corners only
| **alt + shift + o**            |decrease the gaps on the left/right corners only
| **alt + enter**                |zoom but doesn't work

## Layouts  

| Keys                       | Usage
|---                         |---
| **alt + shift + f**            |set current window to full screen mode
| **alt + t**                    |set the layout to tiling mode
| **alt + f**                    |set the layout to floating mode
| **alt + m**                    |set the layout to monocle mode
| **alt + space**                |set the layout to the next/prev
| **alt + shift + space**        |togglefloating (no clue)
